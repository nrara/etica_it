class ListModel{
  String toDoTitle;
  String startDate;
  String endDate;
  String time;
  String status;

  ListModel({
    this.toDoTitle,
    this.startDate,
    this.endDate,
    this.status,
    this.time
});
}

final List<ListModel> list =[
  ListModel(
    toDoTitle: 'Automated Testing Scrip',
    startDate: '21 April 2019',
    endDate: '21 April 2020',
    time: '23 hrs 22 min',
    status: 'Incomplete',
  ),
  ListModel(
    toDoTitle: 'Automated Testing Scrip',
    startDate: '21 June 2019',
    endDate: '21 April 2020',
    time: '23 hrs 22 min',
    status: 'Complete',
  ),
];
