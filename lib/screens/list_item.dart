import 'package:flutter/material.dart';
import 'package:flutter_etica/models/list_model.dart';
import 'package:flutter_etica/resources/colors.dart';
import 'package:flutter_etica/screens/add_new.dart';
import 'package:get/get.dart';

class ListItem extends StatefulWidget {
  String toDoTitle = "";
  String startDate = "";
  String endDate = "";

  ListItem({Key key, this.toDoTitle, this.startDate, this.endDate})
      : super(key: key);

  String title = 'To-Do List';

  @override
  _ListItemState createState() => _ListItemState();
}

class _ListItemState extends State<ListItem> {
  bool _isChecked = false;
  String toDoTitle = "";
  String startDate = "";
  String endDate = "";
  List<ListModel> todoList;

  void _nextPage() {
    setState(() {
      Get.to(() => AddNew());
    });
  }

  @override
  void initState() {
    super.initState();

    todoList = new List();

    // add dummy
    todoList.add(ListModel(
      toDoTitle: 'Automated Testing Scrip',
      startDate: '21 April 2019',
      endDate: '21 April 2020',
      time: '23 hrs 22 min',
      status: 'Incomplete',
    ));

    todoList.add(ListModel(
      toDoTitle: 'Automated Testing Scrip',
      startDate: '2 June 2019',
      endDate: '21 April 2020',
      time: '23 hrs 22 min',
      status: 'Complete',
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: CustomColors.mustard,
        title: Text(
          widget.title,
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: card(),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.orange,
        onPressed: _nextPage,
        tooltip: 'Next Page',
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  card() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: double.maxFinite,
            child: ListView.builder(
                itemCount: todoList.length,
                itemBuilder: (BuildContext context, int index) {
                  ListModel listModel = todoList[index];
                  return Container(
                    width: double.maxFinite,
                    child: InkWell(
                      onTap: () {
                        Get.to(
                          AddNew(
                              toDoTitle: toDoTitle,
                              startDate: startDate,
                              endDate: endDate),
                        );
                      },
                      child: Card(
                        elevation: 2,
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    listModel.toDoTitle,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0),
                                  ),
                                  SizedBox(height: 10),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Start Date',
                                              style:
                                                  TextStyle(color: Colors.grey),
                                            ),
                                            SizedBox(height: 5.0),
                                            Text(
                                              listModel.startDate,
                                              // listModel.startDate,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            )
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'End Date',
                                              style:
                                                  TextStyle(color: Colors.grey),
                                            ),
                                            SizedBox(height: 5.0),
                                            Text(
                                              listModel.endDate,
                                              // listModel.endDate,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Flexible(
                                        flex: 1,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Time Left',
                                              style:
                                                  TextStyle(color: Colors.grey),
                                            ),
                                            SizedBox(height: 5.0),
                                            Text(
                                              '23 hrs 22 min',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(5.0),
                                  bottomRight: Radius.circular(5.0),
                                ),
                                color: CustomColors.lightYellow,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 16.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          'Status',
                                          style: TextStyle(
                                            color: Colors.grey,
                                          ),
                                        ),
                                        SizedBox(width: 5),
                                        Text(
                                          // '${listData[index]['status']}',
                                          'Incomplete',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          'Tick if completed',
                                          style: TextStyle(
                                            color: Colors.grey,
                                          ),
                                        ),
                                        Checkbox(
                                            value: _isChecked,
                                            onChanged: (bool val) {
                                              setState(() {
                                                _isChecked = val;
                                              });
                                            })
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ),
        )
      ],
    );
  }
}
