import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_etica/resources/colors.dart';
import 'package:flutter_etica/screens/list_item.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class AddNew extends StatefulWidget {
  String toDoTitle = "";

  String startDate = "";
  String endDate = "";

  AddNew({Key key, this.toDoTitle, this.startDate, this.endDate})
      : super(key: key);
  final String title = 'Add new To-Do List';

  @override
  _AddNewState createState() => _AddNewState();
}

class _AddNewState extends State<AddNew> {
  String toDoTitle = "";
  String startDate = "";
  String endDate = "";

  DateTime selectDate = DateTime.now();
  DateTime date;

  final _titleInput = TextEditingController();
  final _startDate = TextEditingController();
  final _endDate = TextEditingController();

  bool _titleValidate = true;
  bool _startDateValidate = true;
  bool _endDateValidate = true;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: CustomColors.mustard,
          title: Text(
            widget.title,
            style: TextStyle(color: Colors.black),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Column(
          children: [
            Flexible(
              flex: 2,
              child: ListView(
                children: [
                  Form(
                    key: _formKey,
                    child: textForm(),
                  ),
                ],
              ),
            ),
            Flexible(
              flex: 1,
              child: Stack(
                fit: StackFit.loose,
                children: [
                  Positioned(
                    bottom: 0,
                    height: 50.0,
                    width: MediaQuery.of(context).size.width,
                    child: RaisedButton(
                      color: Colors.black,
                      child: Text(
                        'Create Now',
                        style: TextStyle(fontSize: 16.0, color: Colors.white),
                      ),
                      onPressed: () {
                        _titleInput.text.isEmpty
                            ? _titleValidate = false
                            : _titleValidate = true;
                        _startDate.text.isEmpty
                            ? _startDateValidate = false
                            : _startDateValidate = true;
                        _endDate.text.isEmpty
                            ? _endDateValidate = false
                            : _endDateValidate = true;

                        toDoTitle = _titleInput.text;
                        startDate = _startDate.text;
                        endDate = _endDate.text;

                        if (_titleValidate &&
                            _startDateValidate &&
                            _endDateValidate) {
                          Get.to(ListItem(
                            startDate: startDate,
                            toDoTitle: toDoTitle,
                            endDate: endDate,
                          ));
                        } else {
                          setState(() {});
                        }
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  textForm() {
    return Center(
      child: Container(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(24.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'To-Do Title',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  SizedBox(height: 5.0),
                  TextFormField(
                    maxLines: 5,
                    autocorrect: false,
                    controller: _titleInput,
                    keyboardType: TextInputType.text,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      labelStyle: TextStyle(color: Colors.grey),
                      hintText: 'Please key in your To-Do title here',
                      errorText: _titleValidate ? null : 'Title name needed',
                      floatingLabelBehavior: FloatingLabelBehavior.auto,
                      border: new OutlineInputBorder(
                        borderSide: new BorderSide(color: Colors.black),
                      ),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    'Start Date',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  SizedBox(height: 5.0),
                  TextFormField(
                    controller: _startDate,
                    onTap: () async {
                      // stop keyboard from appearing
                      FocusScope.of(context).requestFocus(new FocusNode());

                      // show date picker
                      await _selectDate(context);
                      _startDate.text = DateFormat('yyyy/MM/dd').format(date);
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Select a date',
                      errorText:
                          _startDateValidate ? null : 'Start Date is Required.',
                      suffixIcon: Icon(Icons.keyboard_arrow_down),
                      contentPadding: EdgeInsets.symmetric(
                        vertical: 0.0,
                        horizontal: 12.0,
                      ),
                    ),
                    validator: (String value) {
                      print('date: ${date.toString()}');
                      if (value.isEmpty) {
                        return 'Start Date is Required.';
                      }
                      return null;
                    },
                    onSaved: (String val) {
                      startDate = val;
                    },
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    'Estimate End Date',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  SizedBox(height: 5.0),
                  TextFormField(
                    controller: _endDate,
                    onTap: () async {
                      // stop keyboard from appearing
                      FocusScope.of(context).requestFocus(new FocusNode());

                      // show date picker
                      await _selectDate(context);
                      _endDate.text = DateFormat('yyyy/MM/dd').format(date);
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Select a date',
                      errorText: _endDateValidate
                          ? null
                          : 'Estimate End Date is Required.',
                      suffixIcon: Icon(Icons.keyboard_arrow_down),
                      contentPadding: EdgeInsets.symmetric(
                        vertical: 0.0,
                        horizontal: 12.0,
                      ),
                    ),
                    validator: (String value) {
                      print('date: ${date.toString()}');
                      if (value.isEmpty) {
                        return 'Estimate End Date is Required.';
                      }
                      return null;
                    },
                    onSaved: (String val) {
                      endDate = val;
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final now = DateTime.now();
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: date ?? now,
        firstDate: now,
        lastDate: DateTime(2101));
    if (picked != null && picked != date) {
      print('date $picked');
      setState(() {
        date = picked;
      });
    }
  }
}
