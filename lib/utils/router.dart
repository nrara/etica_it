import 'package:flutter/material.dart';
import 'package:flutter_etica/screens/add_new.dart';
import 'package:flutter_etica/screens/list_item.dart';
import 'package:flutter_etica/screens/splash.dart';

class PageRouter{
  static Route<dynamic>generateRoute(RouteSettings settings){
    switch (settings.name){
      case '/':
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case '/list':
        return MaterialPageRoute(builder: (_) => ListItem());
      case '/add':
        return MaterialPageRoute(builder: (_) => AddNew());
    }
  }
}